import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ReactJSONViewer from 'react-json-viewer';
import Collapsible from 'react-collapsible';
import DefaultUpload from './DefaultUpload';
import axios from 'axios';

const localstorage_key = 'savedJson'

class JsonUpload extends Component {
  constructor() {
    super();
    this.state = {
      userJson: {},
    };

    this.preSetJSONs = [
        {
            name: "Announcement Event",
            location: {
                address: "Tempe, AZ",
                latitude: 33.4255,
                longitude: -111.9400,
            },
            date: "9-23-2018",
            time: "16:00 to 18:00",
            YouTubeURL: "https://www.youtube.com/watch?v=oHg5SJYRHA0",
            group: [
                {
                    name: "user1",
                    email: "user1@gmail.com"
                },
                {
                    name: "user2",
                    email: "user2@yahoo.com"
                },
                {
                    name: "user3",
                    email: "user3@gmail.com"
                }
            ]

        }
    ];
  }

  componentDidMount() {
    this.btnClick({
      currentTarget: {
        dataset: {
          jsonId: 0,
        },
      },
    });
  }

  btnClick(e) {
    this.refs.textarea.value = JSON.stringify(this.preSetJSONs[e.currentTarget.dataset.jsonId], null, "  ");
    this.setState({
      userJson: this.preSetJSONs[e.currentTarget.dataset.jsonId],
    });
  }

  validateJson() {
    try {
      var x = JSON.parse(this.refs.textarea.value);
      this.setState({
        userJson: x,
      });
    } catch (ex) {
      this.setState({
        userJson: {
          errorType: "Parse  Error",
          error: `${ex}`,
        },
      });
    }
  }

  saveJson(e) {
    window.localStorage.setItem(localstorage_key,this.refs.textarea.value);
  }

  loadJson(e) {
    var x = window.localStorage.getItem(localstorage_key)
    this.setState({ userJson: x });
    this.refs.textarea.value = x;
    this.validateJson();
  }

  keyup() {
    this.validateJson();
  }

  getUpload()
  {
    axios.post('/uploadHandler', {
      filename: "json-test-file.json",
      contents: JSON.stringify(this.refs.textarea.value)
    })
    .then (function (response) {
        console.log(response);
    })
    .catch (function (error) {
        console.log(error);
    })
  }

  dataStoreWrite() {
    axios.get('/sendStore')
    .then(response => console.log(response))
  }

  bucketLister() {
    axios.get('/listBucketItems')
    .then(response => console.log(response))
  }

  render() {
    return (
      <div id="App">
        <h1 id="App-header">Bucket Upload Test</h1>
        <div id="App-window">
          <div id="App-info">
            <h3>Edit JSON file</h3>
            <p> Enter text data, to be parsed as JSON</p>
          </div>
          <Collapsible trigger="Click to input JSON">
            <div id="Button-bar">
              <button
                data-json-id="0"
                style={{ padding: 5, marginLeft: 10 }}
                onClick={this.btnClick.bind(this)}>
                Reset to Default
              </button>
              <button
                id="saveButton"
                style={{ padding: 5, marginLeft: 10 }}
                onClick={this.saveJson.bind(this)}>
                Save JSON to Local Storage
              </button>
              <button
                id="loadButton"
                style={{ padding: 5, marginLeft: 10 }}
                onClick={this.loadJson.bind(this)}>
                Load JSON to Local Storage
              </button>
            </div>
            <Collapsible id="Collapsible-inner" trigger="JSON Text" open="true">
              <textarea
                style={{
                  fontSize: 15,
                  fontFamily: "monospace"
                }}
                ref="textarea"
                onKeyUp={this.keyup.bind(this)}
                placeholder="Enter JSON here"
                name=""
                id=""
                cols="80"
                rows="20"/>
            </Collapsible>
            <Collapsible
              id="Collapsible-inner"
              trigger="JSON Viewer"
              open="true">
              <ReactJSONViewer json={this.state.userJson} />
            </Collapsible>
          </Collapsible>
          <div id="App-info">
            <h3>Upload above JSON File to bucket</h3>
            <p> Enter filename</p>
            <input
              style={{ padding: 5, marginLeft: 10 }}
              type="text"
              value="name.json"
              ref="jsoninput"/>
            <button
              style={{ padding: 5, marginLeft: 10 }}
              onClick={this.getUpload.bind(this)}>
              Click to upload JSON file
            </button>

            <h3>Drag and Drop, Upload File</h3>
            <p>Upload image or JSON, only upload files smaller than 1 MB</p>
            <DefaultUpload />
          <h3>Bucket Lister</h3>
            <button
              onClick={this.bucketLister.bind(this)}>
              Log Bucket Items in Console
            </button>
          </div>
          <div id="App-info">

            <h3>Datastore Upload</h3>
            <p>Upload Default Event</p>
            <button
              onClick={this.dataStoreWrite.bind(this)}>
              Click to write Event to datastore
            </button>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<JsonUpload />, document.getElementById('app'));
